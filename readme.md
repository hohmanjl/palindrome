#Palindrome

Author: James Hohman

Date: 7/19/2017

Simple palindrome finder via recursion.

Pass in a dictionary and the function will return
all the subset of palindromes that the
string contains.

```
>>> from palindrome import get_palindrome
>>> p = 'ABCBA'
>>> result = get_palindrome(p)
>>> print(result)
>>> { 0: 'ABCBA', 1: 'BCB', 2: 'C' }
```

Start at a different index.
```
>>> from palindrome import get_palindrome
>>> p = 'ABCBA'
>>> result = get_palindrome(p, 1)
>>> print(result)
>>> { 1: 'BCB', 2: 'C' }
```

Run the tests.
```
> test.py
.......
----------------------------------------------------------------------
Ran 7 tests in 0.001s

OK
```

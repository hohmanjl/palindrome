#! python3

"""
Author: James Hohman
Date: 7/19/2017

get_palindrome test.
"""


def get_palindromes(sentence, ind=0, results=None):
    """
    Returns all palindromes in the sentence as a results dictionary.

    :param sentence:
    :param ind:
    :param results:
    :return results:
    """
    midpoint = len(sentence) // 2
    if results is None:
        results = {}

    if ind > midpoint:
        raise RuntimeError('Index cannot exceed midpoint.')

    if ind == midpoint:
        if len(sentence) % 2 == 1:
            # self similar case
            results[ind] = sentence[ind:midpoint + 1]

        return results
    elif ind < midpoint:
        # prevent from calling itself too many times with midpoint check
        r_ind = len(sentence) - ind - 1

        if sentence[ind] == sentence[r_ind]:
            results[ind] = sentence[ind:r_ind + 1]
        else:
            # mismatch, clear result set
            results.clear()

        return get_palindromes(sentence, ind + 1, results)

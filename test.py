#! python3

import unittest
from palindrome import get_palindromes


class TestPalindrome(unittest.TestCase):
    def test_palindrome_01(self):
        sentence = 'ABCDCBA'
        result = {0: 'ABCDCBA', 1: 'BCDCB', 2: 'CDC', 3: 'D'}

        ret_val = get_palindromes(sentence)

        # Assert Postcondition
        self.assertEqual(ret_val, result)

    def test_palindrome_02(self):
        sentence = 'A'
        result = {0: 'A'}

        ret_val = get_palindromes(sentence)

        self.assertEqual(ret_val, result)

    def test_palindrome_03(self):
        sentence = 'AA'
        result = {0: 'AA'}

        ret_val = get_palindromes(sentence)

        self.assertEqual(ret_val, result)

    def test_palindrome_04(self):
        sentence = 'AAA'
        result = {0: 'AAA', 1: 'A'}

        ret_val = get_palindromes(sentence)

        self.assertEqual(ret_val, result)

    def test_palindrome_05(self):
        sentence = 'ABCDCBQ'
        result = {1: 'BCDCB', 2: 'CDC', 3: 'D'}

        ret_val = get_palindromes(sentence)

        self.assertEqual(ret_val, result)

    def test_palindrome_06(self):
        sentence = 'ABCDCQA'
        result = {2: 'CDC', 3: 'D'}

        ret_val = get_palindromes(sentence)

        self.assertEqual(ret_val, result)

    def test_palindrome_exceptions_01(self):
        sentence = 'ABCDCQA'

        with self.assertRaises(RuntimeError):
            get_palindromes(sentence, 99)

    def test_palindrome_memo_input(self):
        sentence = 'ABCDCBA'
        result = {0: 'ABCDCBA', 1: 'BCDCB', 2: 'CDC', 3: 'D'}

        memo = {}

        ret_val = get_palindromes(sentence, 0, memo)

        self.assertEqual(ret_val, result)
        self.assertIs(memo, ret_val)

    def test_palindrome_index_input(self):
        sentence = 'ABCDCBA'
        result = {2: 'CDC', 3: 'D'}

        ret_val = get_palindromes(sentence, 2)

        self.assertEqual(ret_val, result)

    def test_palindrome_no_optional_args(self):
        sentence = 'ABCDCBA'
        result = {0: 'ABCDCBA', 1: 'BCDCB', 2: 'CDC', 3: 'D'}

        ret_val = get_palindromes(sentence)

        self.assertEqual(ret_val, result)


if __name__ == '__main__':
    unittest.main()
